! This file is composed of all the subfiles since it is a short file
! With element definition, sequence definition, beam definition and options

! Element definition

d1: drift, l=0.5*m;

! by setting the aperture type to circular vacuum we get a drift
! without any beam pipe - invisible by default - use --vis_debug to see it

airCylinder: drift, l=50*cm, apertureType="circularVacuum", vacuumMaterial="air", aper1=40*cm;

! by defining a rectangular collimator with no opening, we get a square block

blueish: newcolour, red=0, green=128, blue=255, alpha=0.3;
target: rcol, l=10*cm, material="Water", colour="blueish"; ! The colour is only for the visualisation

! similarly a recangular block of a given material but with a casing as provided by the beam pipe
targetBox: drift, l=1*m, apertureType="rectangular", beampipeThickness=1*cm, aper1=20*cm, aper2=40*cm, vacuumMaterial="water";

! Sequence definition

l1: line=(d1, airCylinder, target, airCylinder);
use, l1;

! Beam definition

beam, particle="gamma",
      energy=300*keV,
      distrType = "gauss", ! Gaussian beam definition, others are possible
      sigmaX = 10*um, ! Horizontal beamsize
      sigmaXp = 0,    ! Horizontal divergence
      sigmaY = 10*um, ! Vertical beamsize
      sigmaYp = 0,    ! Vertical divergence
      sigmaE = 10-1;  ! Relative energy spread

! Option definition

option, physicsList="em", ! have a look to the physicslist in the documentation
	stopSecondaries = 0, ! If 1 do not track the secondary particles by default it's 0
	ngenerate=1000; ! Generate 1000 photons automatically, overwritten by the command ngenerate when running bdsim

sample, all; ! Sample all elements, otherwise you can simply sample a given element by specifying it, see the doc